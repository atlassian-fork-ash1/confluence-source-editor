/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

define('source-editor/source-editor-search', [
    'jquery',
    'source-editor/source-editor-cm-search-adapter'
], function (
    $,
    SearchAdapter
) {
    var $d = $(document);
    var searchService;
    var $findInput;
    var $replaceInput;
    var $prev;
    var $next;
    var $replace;
    var $replaceAll;
    var $regex;
    var $matchCase;

    function enableButtons(enable) {
        var i;
        var l;
        var buttons = [$prev, $next, $replace, $replaceAll];
        for (i = 0, l = buttons.length; i < l; i++) {
            enableButton(buttons[i], enable);
        }
    }

    function enableButton($button, enable) {
        $button.toggleClass('disabled', !enable);
        $button.closest('.toolbar-item').toggleClass('disabled', !enable);
    }

    function defaultModifier(e) {
        if ($.browser.mac) {
            return e.metaKey;
        }
        return e.ctrlKey;
    }

    function focusFind() {
        $findInput.focus();
    }

    const onInit = function (event, loadedData) {
    // Stuff only bound once, and doesn't need to be unbound if dialog hidden.

        // Implementation for CodeMirror 2. Replace this to use an alternate editor
        searchService = SearchAdapter(loadedData.editor);
        $findInput = $('#source-editor-search-toolbar-find-text');
        $replaceInput = $('#source-editor-search-toolbar-replace-text');
        $prev = $('#source-editor-search-toolbar-prev');
        $next = $('#source-editor-search-toolbar-next');
        $replace = $('#source-editor-search-toolbar-replace');
        $replaceAll = $('#source-editor-search-toolbar-replace-all');
        $regex = $('#source-editor-search-toolbar-regex');
        $matchCase = $('#source-editor-search-toolbar-match-case');

        var lastState = {
            findText: '',
            //            lastReplaceInput: '',
            regex: false,
            matchCase: false
        };

        function refreshSearch(e) {
            var text = $findInput.val();
            var regex = $regex.attr('checked');
            var matchCase = $matchCase.attr('checked');
            if (lastState.findText !== text || lastState.regex !== regex || lastState.matchCase !== matchCase) {
                lastState.findText = text;
                lastState.regex = regex;
                lastState.matchCase = matchCase;
                if (text.length) {
                    enableButtons(true);
                    searchService.startFind(text, regex, matchCase);
                } else {
                    enableButtons(false);
                    searchService.clearSearch();
                }
            }
        }

        $findInput.bind('keyup.source-editor-search', refreshSearch);
        $regex.bind('click.source-editor-search', refreshSearch);
        $matchCase.bind('click.source-editor-search', refreshSearch);

        $findInput.bind('keydown.source-editor-search', function (e) {
            if (e.which == 13) {
                if (e.shiftKey) {
                    searchService.findPrev();
                } else {
                    searchService.findNext();
                }
                e.preventDefault();
            }
        });
        $replaceInput.bind('keydown.source-editor-search', function (e) {
            if (e.which == 13) {
                searchService.replace($replaceInput.val());
            }
        });

        $prev.bind('click.source-editor-search', function (e) {
            searchService.findPrev();
            e.preventDefault();
        });

        $next.bind('click.source-editor-search', function (e) {
            searchService.findNext();
            e.preventDefault();
        });

        $replace.bind('click.source-editor-search', function (e) {
            searchService.replace($replaceInput.val());
            e.preventDefault();
        });

        $replaceAll.bind('click.source-editor-search', function (e) {
            searchService.replaceAll($replaceInput.val());
            e.preventDefault();
        });
    };

    const onOpen = function (event, data) {
    // Create new instance is open to reset any search state.
        searchService = SearchAdapter(data.editor);

        $d.bind('keydown.source-editor-search', function (e) {
            if (defaultModifier(e)) {
                if (e.which === 70) { // F
                    focusFind();
                    e.preventDefault();
                } else if (e.which === 71 && !e.shiftKey) { // G
                    searchService.findNext();
                    e.preventDefault();
                } else if (e.which === 71 && e.shiftKey) { // G
                    searchService.findPrev();
                    e.preventDefault();
                } else if (e.which === 82 && !e.shiftKey) { // R
                    searchService.replace($replaceInput.val());
                    e.preventDefault();
                } else if (e.which === 82 && e.shiftKey) { // R
                    searchService.replaceAll($replaceInput.val());
                    e.preventDefault();
                }
            }
        });
    };

    const onClose = function () {
        $d.unbind('.source-editor-search');
    };

    return function () {
        $d.bind('source-editor-init', onInit);
        $d.bind('source-editor-opened', onOpen);
        $d.bind('source-editor-closed', onClose);
    };
});
