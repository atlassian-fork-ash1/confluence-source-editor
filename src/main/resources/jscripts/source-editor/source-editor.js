/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

define('source-editor/source-editor', [
    'ajs',
    'jquery',
    'window',
    'document',
    'raphael',
    'confluence/api/logger',
    'confluence/legacy',
    'confluence/meta',
    'source-editor/source-editor-search'
], function (
    AJS,
    $,
    window,
    document,
    Raphael,
    logger,
    Confluence,
    Meta,
    SourceEditorSearch
) {
    const $d = $(document);
    const $w = $(window);

    var $header;
    var $content;
    var $scroll;
    var $panel;

    var px = /(\d*)(px)?/;
    var dialog;
    var editor;
    var throbber;

    function createToolbar() {
        var helpGroup = $('#rte-button-help').parent('ul');
        helpGroup.before(Confluence.SourceEditor.Templates.toolbar());

        var sourceEditorButton = $('#rte-source-editor-button');

        sourceEditorButton.click(function (e) {
            e.preventDefault();
            openSourceEditor();
        });

        // hacky stuff to fix toolbar resizing.
        addPixels($('#rte-toolbar .aui-toolbar2-primary'), 'margin-right', 45);
    }

    function addPixels(el, cssStyle, add) {
        var match = px.exec(el.css(cssStyle));
        var curWidth;
        if (match.length > 1) {
            curWidth = +match[1];
            curWidth += add;
            el.css(cssStyle, curWidth + 'px');
        }
    }

    function hideThrobber() {
        throbber.addClass('hidden');
    }

    function showThrobber() {
        throbber.removeClass('hidden');
        $('input').blur();
    }

    function gotoLineColumn(line, column) {
        editor && editor.setCursor(line - 1, column - 1);
        editor.focus();
    }

    function getDialogSettings() {
    // Keep seperate in case we want to add more than width and height
        return getDialogSize();
    }

    function getDialogSize() {
        var desiredWidth = $w.width() - 80;
        var desiredHeight = $w.height() - 80;
        return {
            width: Math.max(860, desiredWidth),
            height: Math.max(530, desiredHeight)
        };
    }

    function updateSize() {
        var size = getDialogSize();
        dialog.popup.changeSize(size.width, size.height);
        dialog.height = size.height;
        dialog.width = size.width;
        dialog.getPage(0).recalcSize();
        updateEditorSize();
    }

    function updateEditorSize() {
        var height = $panel.height() - $header.height();
        $content.height(height);
        $scroll.height(height);
    }

    function isMinimumVersion() {
        var version = Meta.get('version-number');
        var versionParts = version.split('.');
        if (version.indexOf('SNAPSHOT') >= 0) return true; // always allow snapshot at own peril

        var major = parseInt(versionParts.length > 0 && versionParts[0] || 0);
        var minor = parseInt(versionParts.length > 1 && versionParts[1] || 0);
        var patch = parseInt(versionParts.length > 2 && versionParts[2] || 0);
        // Min version = 4.1.5
        if (major > 4) return true;
        if (major === 4 && minor >= 2) return true;
        if (major === 4 && minor === 1 && patch >= 5) return true;

        return false;
    }

    function showIncorrectVersionMessage() {
        var popup = new AJS.Dialog({
            width: 400,
            height: 150,
            closeOnOutsideClick: true
        });
        var $dialog = popup.popup.element;
        popup.addHeader('Source Editor');
        popup.addPanel('IncorrectVersionPanel', '<p>The source editor requires Confluence <strong>4.1.5</strong> or greater.</p>');
        popup.addSubmit('Ok', function () {
            popup.remove();
        });
        popup.show();
    }

    function openSourceEditor() {
        if (!isMinimumVersion()) {
            // Needed as not everyone reads release notes
            showIncorrectVersionMessage();
            return;
        }


        if (!dialog) {
            dialog = new AJS.Dialog(getDialogSettings());
            dialog.addHeader('Source Editor');
            dialog.addPanel('SourceEditorPanel', 'sourceEditorPanel1');
            dialog.getCurrentPanel().html(Confluence.SourceEditor.Templates.sourceEditorPlaceholder());
            dialog.addSubmit('Apply', saveChanges);
            dialog.addCancel('Cancel', cancelChanges);

            var wrapLines = AJS.Cookie.read('source-editor.wrap-lines') === 'true';

            dialog.popup.element.find('.dialog-button-panel')
                .append(Confluence.SourceEditor.Templates.sourceEditorFooterLeftToolbar({ wrapLines: wrapLines }));

            editor = CodeMirror(function (el) {
                $('#source-editor-placeholder').append(el);
            }, {
                value: '',
                lineNumbers: true,
                mode: 'xml',
                lineWrapping: wrapLines
            });
            throbber = $('<div id="source-editor-throbber"/>');
            Raphael.spinner(throbber[0], 50, '#666');

            $header = $('#source-editor-header');
            $content = $('#source-editor-placeholder');
            $scroll = $content.find('.CodeMirror-scroll');
            $panel = $header.closest('.dialog-panel-body');

            throbber.appendTo($header.closest('.aui-dialog'));

            updateEditorSize();

            $d.trigger('source-editor-init', { editor: editor });
        } else {
            updateSize();
        }
        dialog.show();

        $d.delegate('a.source-editor-location', 'click.source-editor', function () {
            var $a = $(this);
            gotoLineColumn($a.data('line'), $a.data('column'));
        });

        $d.delegate('.aui-message.closeable', 'messageClose.source-editor', function () {
            // Clear render pipeline first
            setTimeout(updateEditorSize, 0);
        });

        $d.delegate('#source-editor-wrap-lines', 'click.source-editor', function () {
            var checkbox = $(this);
            var checked = checkbox.prop('checked');
            editor.setOption('lineWrapping', checked);
            AJS.Cookie.save('source-editor.wrap-lines', checked);
        });

        $w.bind('resize.source-editor', updateSize);

        // prevent tabbing into elements outside of editor
        $d.delegate('*', 'focus.source-editor', function (e) {
            var $target = $(e.target);
            if (!$target.closest('#source-editor').length) {
                $target.blur();
            }
        });

        editor.setValue('');
        showThrobber();
        loadSourceFormat(function (content) {
            editor.setValue(content);
            editor.refresh();
            editor.clearHistory(); // make sure there's no stale undo from other editors or the initial state.
            editor.focus();
            $d.trigger('source-editor-loaded', { editor: editor });
            hideThrobber();
        });

        $d.trigger('source-editor-opened', { editor: editor });

        // Handle hide via escape, etc...
        $d.bind('hideLayer.source-editor', function (e, type, popup) {
            if (dialog.popup === popup) {
                unbindSourceEditor();
            }
        });
    }

    function closeSourceEditor() {
        dialog.hide();
    }

    function unbindSourceEditor() {
        $d.unbind('.source-editor');
        $w.unbind('.source-editor');
        $d.trigger('source-editor-closed', { editor: editor });
    }

    function saveChanges() {
        showThrobber();
        loadEditorFormat(function (content) {
            var ed = AJS.Rte.getEditor();
            ed.undoManager.add();
            ed.setContent(content);
            ed.undoManager.add();
            ed.nodeChanged();
            closeSourceEditor();
            hideThrobber();
        });
    }

    function cancelChanges() {
        closeSourceEditor();
    }

    function loadSourceFormat(done) {
        $.ajax(Meta.get('context-path') + '/rest/sourceeditor/1.0/convert/toStorageFormat', {
            type: 'POST',
            // Need to force UTF-8 encoding - seems server assumes otherwise.
            contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
            data: requestData({
                editorFormat: AJS.Rte.getEditor().getContent()
            }),
            dataType: 'json'
        })
            .done(function (data) {
                clearError();
                done(data.content);
            })
            .fail(function (xhr) {
                displayError('Error loading source', xhr);
            });
    }

    function loadEditorFormat(done) {
        var storageFormat = getSource();
        $.ajax(Meta.get('context-path') + '/rest/sourceeditor/1.0/convert/toEditorFormat', {
            type: 'POST',
            // Need to force UTF-8 encoding - seems server assumes otherwise.
            contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
            data: requestData({
                storageFormat: storageFormat
            }),
            dataType: 'json'
        })
            .done(function (data) {
                clearError();
                done(data.content);
            })
            .fail(function (xhr) {
                displayError('Error validating source', xhr);
            });
    }

    function requestData(base) {
        var contentType = Meta.get('content-type');

        if (contentType === 'comment') {
            // Is view, determine if page or blogpost
            if ($('body').hasClass('view-blog-post')) {
                contentType = 'blogpost';
            } else {
                contentType = 'page';
            }
        }

        if (contentType === 'page') {
            return $.extend({
                pageId: Meta.get('page-id')
            }, base);
        }

        if (contentType === 'template') {
            return $.extend({
                templateId: Meta.get('page-id')
            }, base);
        }

        return $.extend({
            blogId: Meta.get('page-id')
        }, base);
    }

    function getSource(stripNewLines) {
        var source = editor.getValue();
        if (stripNewLines) {
            source = source.replace('\n', '');
        }
        return source;
    }

    function clearError() {
        $('#source-editor-status').empty();
        updateEditorSize();
    }

    function displayError(title, xhr) {
        clearError();

        var responseType = xhr.getResponseHeader('Content-Type');
        var response;
        if (/^application\/json$|^application\/json;.*/.test(responseType)) {
            response = JSON.parse(xhr.responseText);
        } else if (xhr.status == 403) {
            response = {
                message: 'You no longer have access to the source editor.'
            };
        } else {
            response = {
                message: 'Unknown error occurred calling server.'
            };
        }
        var message = '<ul><li>' + AJS.escapeHtml(response.message + ' (' + xhr.status + ')') + '</li>';

        if (typeof response.line === 'number' && typeof response.column === 'number'
            && response.line > 0 && response.column > 0) {
            // Help by repositioning cursor.
            gotoLineColumn(response.line, response.column);
            message += '<li><a href="#" class="source-editor-location" data-line="' + response.line + '" data-column="' + response.column + '">Line : ' + response.line + ', Column : ' + response.column + '</a></li>';
        }

        message += '</ul>';

        AJS.messages.error('#source-editor-status', {
            title: title,
            body: message,
            shadowed: false,
            closeable: true
        });

        hideThrobber();
        updateEditorSize();
    }

    // Init:

    const init = function () {
    // Initialise search
        SourceEditorSearch();

        logger.log('Initialising the Source Editor.');

        if (Meta.get('page-id') == 0) {
            logger.log('Page is a draft - source editing is not supported');
            return;
        }

        logger.log('loading source editor');


        // Add toolbar icon
        createToolbar();

        $d.trigger('source-editor-api-available');
    };

    return {
        init: init,
        open: openSourceEditor,
        save: saveChanges,
        cancel: cancelChanges,
        getSource: getSource,
        getInternalEditor: function () { return editor; }
    };
});
