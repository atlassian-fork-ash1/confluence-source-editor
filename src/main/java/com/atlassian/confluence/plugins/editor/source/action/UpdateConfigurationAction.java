/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.plugins.editor.source.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.editor.source.settings.SourceEditorConfigurationManager;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class UpdateConfigurationAction extends ConfluenceActionSupport {
    private SourceEditorConfigurationManager sourceEditorConfigurationManager;

    private String groups;
    private String allowAll;

    private boolean isAllowAll() {
        return "on".equals(allowAll);
    }

    private List<String> getGroups() {
        List<String> groupList = new ArrayList<>();

        if (groups != null) {
            for (String group : groups.split("\r\n")) {
                if (!"".equals(group)) {
                    groupList.add(group.trim());
                }
            }
        }

        return groupList;
    }

    @Override
    public String execute() {
        sourceEditorConfigurationManager.setAccessGroups(getGroups());
        sourceEditorConfigurationManager.setAllowAllUsers(isAllowAll());
        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public void setAllowAll(final String allowAll) {
        this.allowAll = allowAll;
    }

    @SuppressWarnings("unused")
    public void setGroups(final String groups) {
        this.groups = groups;
    }

    @SuppressWarnings("unused")
    public void setSourceEditorConfigurationManager(final SourceEditorConfigurationManager sourceEditorConfigurationManager) {
        this.sourceEditorConfigurationManager = sourceEditorConfigurationManager;
    }
}
