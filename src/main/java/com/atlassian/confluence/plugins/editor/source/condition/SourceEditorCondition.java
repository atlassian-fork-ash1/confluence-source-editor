/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.plugins.editor.source.condition;

import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.atlassian.confluence.plugins.editor.source.settings.SourceEditorConfigurationManager;
import com.atlassian.sal.api.user.UserManager;

/**
 *
 */
public class SourceEditorCondition extends BaseConfluenceCondition {
    private final SourceEditorConfigurationManager sourceEditorConfigurationManager;
    private final UserManager userManager;

    public SourceEditorCondition(final SourceEditorConfigurationManager sourceEditorConfigurationManager,
                                 final UserManager userManager) {
        this.sourceEditorConfigurationManager = sourceEditorConfigurationManager;
        this.userManager = userManager;
    }

    @Override
    protected boolean shouldDisplay(final WebInterfaceContext webInterfaceContext) {
        return sourceEditorConfigurationManager.hasAccess();
    }
}
